import React, {Component} from "react";
import { Map, TileLayer, Marker, Popup } from "react-leaflet";
import axios from "axios";
import { divIcon, latLngBounds } from "leaflet";
import "leaflet/dist/leaflet.css";


const LOCATIONS = [
    {
        position : [19.064174, 72.833425],
        icon:divIcon({className: 'home-icon',iconSize: [50, 50], popupAnchor:[0,-20]})
    },
    {
        position : [19.051675, 72.830331],
        icon:divIcon({className: 'home-icon1',iconSize: [50, 50], popupAnchor:[0,-20]})
    },
    {
        position : [19.069218, 72.824811],
        icon:divIcon({className: 'home-icon2',iconSize: [50, 50], popupAnchor:[0,-20]})
    }
]

export default class Maps extends Component{
    constructor(props){
        super(props);

        this.state = {
            lat: 30.709624,
            lng: 76.724270,
            zoom: 13,
            dataToShow:null,
            center:[30.709624,76.724270]
        };
    }
    
    componentDidMount(){
        setTimeout(()=>{
            var bounds = latLngBounds([LOCATIONS[0].position, LOCATIONS[1].position, LOCATIONS[2].position]);
            this.map.leafletElement.fitBounds(bounds,{padding:[70,70]});
        },300);
    }

getIscoChronesData(location){
		this.setState({
			apiCallInProgress:true,
			currentGeoJson:null,
            currentTransportData:[]
		},()=>{
			axios({
				method:"GET",
				url:`http://62.210.93.54:8109/transport?lat=${location.lat}&lon=${location.lng}&time_limit=600&vehicle=bike`
			}).then((response)=>{
                console.log(response);
                var dataObject = {
                    vegetation:response.data.peaks.result.vegetation,
                    beach:response.data.peaks.result.beach,
                    ocean:response.data.peaks.result.ocean,
                    river:response.data.peaks.result.river
                };
                var restaurantCount = 0;
                response.data.pois.map((poi)=>{
                    if(poi.doc_type == "triprestaurant"){
                        restaurantCount++;
                    }
                    if(poi.doc_type == "attraction"){
                        dataObject.attraction = poi;
                    }
                });
                dataObject.restaurantCount = restaurantCount;
                this.setState({
                    dataToShow:dataObject
                });
			}).catch((err)=>{
				console.log(err);
			})
		})
    }
    

    render(){
        return(
            <div className="map-wrapper">
                <div className="top-header">
                    <div className="title">
                        Places Nearby
                    </div>
                </div>
                <div className="map-center" onClick={()=>{
                        var bounds = latLngBounds([LOCATIONS[0].position, LOCATIONS[1].position, LOCATIONS[2].position]);
                        this.map.leafletElement.fitBounds(bounds,{padding:[70,70]});
                }}/>
                <Map center={this.state.center} zoom={this.state.zoom} ref={mapnode => this.map = mapnode}>
                    <TileLayer
                    attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
                    url="https://api.mapbox.com/styles/v1/vsvanshi/cjcfwu9cm3g912snuh5v1rpau/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoidnN2YW5zaGkiLCJhIjoiY2l0YnF3Yzl1MDd0YjJvczZiempmczc0NyJ9.PUqEP-Mrl2Fxl82v6jyCOA"
                    />
                    {
                        LOCATIONS.map((location,index)=>{
                            return(
                                <Marker position={location.position} icon={location.icon} key={index} onClick={()=>{
                                    this.setState({
                                        dataToShow:null
                                    },()=>{
                                        this.getIscoChronesData({lat:location.position[0], lng:location.position[1]})
                                    })
                                }}>
                                    <Popup>
                                        <div className="popup-container">
                                            {
                                                this.state.dataToShow != null 
                                                ?
                                                <div className="location-info">
                                                    <div className="row-item">
                                                        <img src={require("../images/beach.svg")} />
                                                        <div className="lbl">Beach Nearby</div>
                                                        <div className="val">
                                                            {
                                                                this.state.dataToShow.beach.exists ? this.state.dataToShow.beach.name : "No"
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row-item">
                                                        <img src={require("../images/vegetation.svg")} />
                                                        <div className="lbl">Vegetation Score</div>
                                                        <div className="val">
                                                            {
                                                                this.state.dataToShow.vegetation.score
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row-item">
                                                        <img src={require("../images/sea.svg")} />
                                                        <div className="lbl">Ocean</div>
                                                        <div className="val">
                                                            {
                                                                this.state.dataToShow.ocean
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="row-item">
                                                        <img src={require("../images/river.svg")} />
                                                        <div className="lbl">River</div>
                                                        <div className="val">
                                                            {
                                                                this.state.dataToShow.river.name
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className="btn" onClick={()=>{
                                                        window.sessionStorage.setItem("ls",true);
                                                        this.props.history.push("/pics");
                                                    }}>
                                                        Select
                                                    </div>
                                                </div>
                                                :
                                                <div className="imsg"> Fetching Data... </div>
                                            }
                                        </div>
                                    </Popup>
                                </Marker>
                            );
                        })
                    }
                </Map>
            </div>
        )
    }
}