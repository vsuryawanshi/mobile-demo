import React, {Component} from "react";
import axios from "axios";
import ReactTooltip from "react-tooltip";

const IMAGES = [
    "https://images.financialexpress.com/2015/09/JatiaHouse-14.jpg",
    "http://www.cititelpenang.com/templates/main/images/penang_page_accommodation_body-bg.jpg?1396021078",
    "https://www.usnews.com/dims4/USNEWS/ccab0b3/2147483647/resize/1200x%3E/quality/85/?url=http%3A%2F%2Fmedia.beam.usnews.com%2F51%2F81%2F7bcf8a914549b6658716e4d2ae1c%2F160829-luxuryhotel-stock.jpg",
    "https://media-cdn.tripadvisor.com/media/photo-s/00/1d/60/93/living-room-inside-property.jpg",
    
    "http://static.punjabkesari.in/multimedia/10_40_531970910shakti%20kapoor%20home%205-ll.jpg",
    "https://cdn.images.express.co.uk/img/dynamic/51/590x/secondary/Inside-the-property-251182.jpg"
];


export default class Pics extends Component{
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress:false,
            currentImageIndex:0,
            locationSelected:true,
            showNotification:false,
            showNotification1:false,
            imgWidth:200,
            imgHeight:200,
            nw:200,
            nh:200,
            notificationText:"",
            imageHeight:"330px",
            notificationHeading:"",
            imgProblem:false,
            step:1,
            roomInfo:"",
            showRoomInfo:false,
            sceneCategories:[],
            clickedSceneCategories:[],
            points:[]
        }
    }

    componentDidMount(){
        setTimeout(()=>{
            this.getImageQuality();
        },200);
    }

    getImageQuality(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://51.15.164.118:6006/imagequality?url=" + IMAGES[this.state.currentImageIndex]
            }).then((response)=>{
                if(response.data.AboutImage.length > 0){
                    //means there is some problem in the image
                    this.setState({
                        showNotification:true,
                        notificationText:response.data.AboutImage[0],
                        apiCallInProgress:false,
                        showThumbsButtons:true,
                        imageHeight:"487px",
                        notificationHeading:"Something not right",
                        imgProblem:true
                    });
                } else {
                    this.setState({
                        showNotification:true,
                        notificationText:"Everything seems fine in the image",
                        apiCallInProgress:false,
                        showThumbsButtons:true,
                        imageHeight:"487px",
                        notificationHeading:"Great work",
                        imgProblem:false
                    });
                }
                
            }).catch((err)=>{
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
        
    }

    continue(){
        if(this.state.step == 1){
            this.setState({
                showNotification:false,
                step:2
            },()=>{
                this.callPlacesApi();
            })
        }
    }


    callPlacesApi(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://62.210.100.210:8472/places?url=" +  IMAGES[this.state.currentImageIndex]
            }).then((response)=>{
                console.log(response.data);
                this.setState({
                    apiCallInProgress:false,
                    showNotification1:true,
                    notificationHeading:"It looks like",
                    sceneCategories:response.data.result.sceneCategories
                });
            }).catch((err)=> {console.log(err)
            
                this.setState({
                    apiCallInProgress:false
                })
            });
        })
    }

    sceneCategoryClicked(name,isLast){
        var stateCopy = this.state.clickedSceneCategories;
        if(stateCopy.indexOf(name) == -1){
            this.setState({
                imageHeight:"330px",
                showRoomInfo:true,
                clickedSceneCategories:[...this.state.clickedSceneCategories,name]
            })
        }
    }

    getSemanticInfo(){
        this.setState({
            apiCallInProgress:true,
            showNotification1:false
        },()=>{
            axios({
                method:"GET",
                url:"http://62.210.100.210:8094/semantic?url=" +  IMAGES[this.state.currentImageIndex]
            }).then((response)=>{
                console.log(response.data);
                let _points = [];
                Object.keys(response.data.tags.objects.coordinates).map((cd)=>{
                    let currentObj = response.data.tags.objects.coordinates[cd];
                    var temp = {};
                    temp.x = currentObj[1];
                    temp.y = currentObj[0];
                    temp.name = cd;
                    _points.push(temp);
                })
                this.setState({
                    apiCallInProgress:false,
                    points:_points
                },()=>{
                    ReactTooltip.rebuild();
                });
            }).catch((err)=> {console.log(err)
            
                this.setState({
                    apiCallInProgress:false
                })
            });   
        })
    }

    render(){
        return(
            <div className="home-wrapper">
            {
                this.state.apiCallInProgress ? 
                <div className="loader-wrap">
                    <img className="load-img" src={require("../images/support.png")}/>
                    <div className="ltext">Please wait...</div>
                </div>
                :
                null
            }
                <div className="title-bar">
                    <div className="left" onClick={()=>{
                        if(this.state.currentImageIndex > 0){
                            this.setState({
                                currentImageIndex:this.state.currentImageIndex-1,
                                showRoomInfo:false
                            },()=>{
                                this.getImageQuality();
                            })
                        }
                    }}>
                        <img src={require("../images/right-arrow.svg")} style={{width:"20px"}}/>
                    </div>
                    
                    <div className="title" onClick={()=>{
                        this.props.history.goBack();
                    }}>
                        <img src={require("../images/map.svg")} style={{width:"30px"}}/>
                    </div>
                    <div className="right" onClick={()=>{
                        if(this.state.currentImageIndex < IMAGES.length -1){
                            this.setState({
                                currentImageIndex:this.state.currentImageIndex+1
                            },()=>{
                                this.getImageQuality();
                            })
                        }
                    }}><img src={require("../images/right-arrow.svg")} style={{width:"20px"}}/></div>
                </div>
                <div className="image" style={{height:this.state.imageHeight}}>
                    <img ref={node => this.image = node} src={IMAGES[this.state.currentImageIndex]} className="ci" onLoad={()=>{
                        this.setState({
                            imgWidth:this.image.width,
                            imgHeight:this.image.height,
                            nw:this.image.naturalWidth,
                            nh:this.image.naturalHeight
                        });
                    }}/>
                    {/* <canvas ref={node => this.canvas = node} className="dcanvas" width={this.state.imgWidth} height={this.state.imgHeight}/> */}
                    <div className="dcanvas" style={{width:this.state.imgWidth, height:this.state.imgHeight}}>
                        {
                            this.state.points.map((pt,indx)=>{
                                var wRatio = parseFloat(this.state.imgWidth / 300).toFixed(3);
                                var hRatio = parseFloat(this.state.imgHeight / 300).toFixed(3);

                                return(
                                    <div className="pt" key={indx} style={{top:hRatio*pt.y, left:wRatio*pt.x}} data-tip={pt.name}/>
                                )
                            })
                        }
                    </div>
                    <div className="add-btn" style={{display:"none"}}/>
                </div>
                {
                    this.state.showRoomInfo ?
                    <div className="room-info">
                        <div className="scene-type">
                            {
                                this.state.clickedSceneCategories.map((csc,index)=>{
                                    return(
                                        <div className="csc" key={index}>{csc}</div>
                                    );
                                })
                            }
                        </div>
                    </div>
                    :
                    null
                }
            <div className={`notification` + (this.state.showNotification ? " shown" : "")}>
                <div className="info-wrap">
                    <div className="notif-content">
                        <div className="notif-title">{this.state.notificationHeading}</div>
                        <div className="notif-txt">{this.state.notificationText}</div>
                    </div>
                    {
                        this.state.showThumbsButtons ? 
                        <div className="thumb-btn-wrap">
                            <div className={`tbtn` +(this.state.imgProblem ? " down" : " up")}/>
                        </div>
                        :
                        null
                    }
                </div>  
                <div className="txt-button" onClick={()=>{
                    this.continue();
                }}>
                    {
                        this.state.imgProblem ? "Still Continue" : "Lets continue"
                    }
                ?
                </div> 
            </div>

            <div className={`notification` + (this.state.showNotification1 ? " shown" : "")}>
                <div className="info-wrap">
                    <div className="thumb-btn-wrap">
                        <img src={require("../images/support.png")}/>
                    </div>
                    <div className="notif-content">
                        <div className="notif-title">{this.state.notificationHeading}</div>
                        <div className="notif-txt">
                            {
                                this.state.sceneCategories.map((sc,index)=>{
                                    return(
                                        <div className="emph" key={index} onClick={()=>{
                                            this.sceneCategoryClicked(sc[0],index == this.state.sceneCategories.length - 1);
                                        }}>{sc[0]}</div> 
                                    );
                                })
                            }
                        </div>
                    </div>
                </div>  
                <div className="txt-button" onClick={()=>{
                    this.getSemanticInfo();
                }}>
                    {
                      "Lets continue"
                    }
                ?
                </div> 
            </div>
            <ReactTooltip />
        </div>
        )
    }
}