import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Pics from "./Pics";
import Maps from "./Maps";

export default class Home extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div id="MSvideoContainer">
                <div id="MSvideoContent">
                <Switch>
                    <Route exact path='/' component={Maps}/>
                    <Route exact path='/pics' component={Pics}/>
                </Switch>
                </div>
            </div>
        );
    }
}