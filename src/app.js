import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import "./styles/main.scss";
import "./images/favicon.ico";

import Home from "./containers/Home";


ReactDOM.render(<BrowserRouter><Home/></BrowserRouter>, document.getElementById("react-root"));